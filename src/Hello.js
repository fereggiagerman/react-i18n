import React from 'react'
import { useTranslation } from 'react-i18next'

const Hello = () => {
  const { t, i18n } = useTranslation()

  return (
    <>
      <div>
        {t('HELLO.LABEL', { param: t('HELLO.WORLD') })}
      </div><br />
      <div>{t('HELLO.DATE', { date: new Date() })}</div><br />
    </>
  )
}

export default Hello