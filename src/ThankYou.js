import React from 'react'
import { withTranslation } from 'react-i18next'

const ThankYou = ({ t }) => {

  const getQuantityLabel = () => {
    const randomNumber = Math.floor(Math.random() * 100);
    return t(randomNumber % 2 === 0 ? 'THANKYOU.QUANTITY.PAR_ITEMS' : 'THANKYOU.QUANTITY.ODD_ITEMS', { count: randomNumber })
  }

  return (
    <>
      <div>
        {t('THANKYOU.LABEL')}
      </div>
      <div>
        <p>{getQuantityLabel()}</p>
      </div>
    </>
  )
}

export default withTranslation()(ThankYou)
