import React from 'react'
import { useTranslation } from 'react-i18next'

const LanguageSelector = () => {
  const { t, i18n } = useTranslation()

  const changeLanguage = (event) => {
    i18n.changeLanguage(event.target.value)
  }

  return (
    <div onChange={changeLanguage}>
      <input type="radio" value="es" name="language" defaultChecked /> {t("LANGUAGES.ES")}
      <input type="radio" value="en" name="language"/> {t("LANGUAGES.EN")}
      <input type="radio" value="br" name="language"/> {t("LANGUAGES.BR")}
    </div>
  )
}

export default LanguageSelector